# DSAD Segmentation
This repo contains the code for our paper "One model to use them all: training a segmentation model with complementary datasets" (IPCAI 24).



## Results

| Trainset   | Trial      | Abd       | Col       | Liv       | Pan        | Sma       | Sto       | mean      |
|------------|------------|-----------|-----------|-----------|------------|-----------|-----------|-----------|
| Binary     | IL (ours)  | 0.892     | **0.782** | 0.741     | 0.364      | **0.849** | **0.709** | **0.723** |
| Binary     | EN (lower) | **0.895** | 0.729     | **0.745** | **0.373**  | 0.826     | 0.503     | 0.679     |
|            |            |           |           |           |            |           |           |           |
| MultiCLass | FS (upper) | **0.764** | **0.451** | **0.521** | 0.170      | 0.122     | **0.719** | **0.458** |
| MultiCLass | IL (ours)  | 0.730     | 0.381     | 0.520     | **0.184**  | **0.338** | 0.578     | 0.445     |
| MultiCLass | EN (lower) | 0.673     | 0.320     | 0.490139  | 0.364      | 0.177     | 0.699     | 0.416     |

**Table 1** Every row represents one of the trials, fully supervised (FS), ensemble (EN), and implicit labelling (IL), trained on the binary and multi-class sets.
The highest value per column and trainset is highlighted in bold. The classes Abdominal Wall, Colon, Liver, Pancreas, Small Intestine, and Stomach
are abbreviated by their first three letter

---

| Trainset   | Trial      | Bac       | Abd       | Col       | Liv       | Pan       | Sma       | Sto       | mean      |
|------------|------------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|
| Binary     | IL (ours)  | 0.932     | 0.578     | **0.472** | 0.222     | **0.386** | **0.386** | **0.709** | **0.543** |
| Binary     | EN (lower) | **0.955** | **0.590** | 0.363     | **0.375** | 0.335     | 0.214     | 0.503     | 0.476     |
|            |            |           |           |           |           |           |           |           |           |
| MultiCLass | FS (upper) | **0.970** | **0.800** | **0.450** | **0.521** | **0.679** | **0.913** | **0.719** | **0.783** |
| MultiCLass | IL (ours)  | 0.905     | 0.632     | 0.251     | 0.650     | 0.306     | 0.455     | 0.578     | 0.540     |
| MultiCLass | EN (lower) | 0.931     | 0.728     | 0.360     | 0.387     | 0.239     | 0.304     | 0.699     | 0.512     |

**Table 2** Every row represents one of the trials, fully supervised (FS), ensemble (EN), and implicit labelling (IL), trained on the binary and multi-class sets.
The highest value per column and trainset is highlighted in bold. The classes Background, Abdominal Wall, Colon, Liver, Pancreas, Small Intestine,
and Stomach are abbreviated by their first three letters.

---

| Trial                         | Abd   | Col   | Liv   | Pan   | Sma   | Sto   | mean  |
|-------------------------------|-------|-------|-------|-------|-------|-------|-------|
| Separate models of EN         | 0.894 | 0.790 | 0.786 | 0.400 | 0.866 | 0.701 | 0.740 |
| IL (ours)                     | 0.892 | 0.782 | 0.741 | 0.364 | 0.849 | 0.709 | 0.723 |
| IL w/o additional neg. labels | 0.897 | 0.766 | 0.783 | 0.326 | 0.832 | 0.633 | 0.706 |

**Table 3** The first row shows the performance of each separate model in the ensemble before applying the argmax.
The last row shows a variant of the IL approach if no additional negative samples are inferred. The classes
Abdominal Wall, Colon, Liver, Pancreas, Small Intestine, and Stomach are abbreviated by their first three
letters


## Usage
In all scripts given paths may need adaption.

### Data Structure

The dataset is structured as provided by [DSAD](https://www.synapse.org/#!Synapse:syn30368675/wiki/617677)

This repo contains:

- models: trained models
- src: source code to train and evaluate the models
- README.md: this file
- requirements.txt: Needed packages

### Setup

To install the requirements run

```bash
pip3 install -r requirements.txt
```

### Train

To train the models run

```bash
cd src/
python3 train_fss.py  # to train the FS model on the binary subsets
python3 train_multimodel.py  # to train the EN models on the binary subsets
python3 train_singlemodel.py  # to train the IL model  on the binary subsets
python3 train_fssds_multimodel.py  # to train the EN models on the multi-class subset
python3 train_fssds_singlemodel.py  # to train the IL model on the multi-class subset
python3 train_singlemodel_nIL.py  # to train the IL model without additional negative labels on the binary subsets (ablation)
```

### Evaluation

To evaluate the model performance run

```bash
cd src/
python3 infer_gt.py  # to prepare groundtruth masks
python3 infer_fss.py  # to infer the FL model on the multi-class subset
python3 infer_multimodel.py  # to infer the EN models on the binary subsets
python3 infer_singlemodel.py  # to infer the IL model on the binary subsets
python3 infer_fssds_multimodel.py  # to infer the EN models on the multi-class subset
python3 infer_fssds_singlemodel.py  # to infer the IL model on the multi-class subset
python3 infer_singlemodel_nIL.py  # to infer the IL model without additional negative labels on the binary subsets (ablation)
python3 evaluate.py  # calculate the metrics on the inferred masks
```

To evaluate the model speed run

```bash
cd src/
python3 measure_inferencespeen.py
```

## Citing
If you use this code or models please cite:

```bibtex
@article{jenke2024one,
  title={One model to use them all: training a segmentation model with complementary datasets},
  author={Jenke, Alexander C and Bodenstedt, Sebastian and Kolbinger, Fiona R and Distler, Marius and Weitz, J{\"u}rgen and Speidel, Stefanie},
  journal={International Journal of Computer Assisted Radiology and Surgery},
  pages={1--9},
  year={2024},
  publisher={Springer}
}
```