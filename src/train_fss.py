import os
from datetime import datetime
from shutil import copy2

import numpy as np
import torch
from sklearn.metrics import f1_score
from torch import optim, nn
from torch.utils.data import ConcatDataset, DataLoader
from torch.utils.tensorboard import SummaryWriter
from torchvision.models.segmentation import deeplabv3_resnet50, DeepLabV3_ResNet50_Weights
from torchvision.models.segmentation.deeplabv3 import DeepLabHead
from tqdm import tqdm

from dsad import train_transform, FSS_Label as Label, DSAD_FSS
from split import split

ROOT = "/path/to/dataset/"
NAME = "FSS"
N_CLASSES = 7
N_EPOCHS = 100
BATCH_SIZE = 8
LR = 1e-4

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def get_dataset():
    trainsets = [DSAD_FSS(ROOT, video, transform=train_transform) for video in split['train']]
    validsets = [DSAD_FSS(ROOT, video) for video in split['valid']]

    # pos = np.zeros(7)
    # for ds in trainsets:
    #     pos += ds.get_npos()

    pos = np.asarray([2.32224743e+08, 2.57002790e+07, 6.22646300e+06, 3.46709100e+06, 6.99731000e+05, 1.15238500e+06,
           1.33171480e+07])
    print("n_pos", pos)

    trainloader = DataLoader(ConcatDataset(trainsets), batch_size=BATCH_SIZE, num_workers=8, shuffle=True,
                             pin_memory=True, drop_last=True)
    validloader = DataLoader(ConcatDataset(validsets), batch_size=BATCH_SIZE * 2, num_workers=8, shuffle=False,
                             pin_memory=True)

    return trainloader, validloader, torch.tensor(-pos / pos.sum()).softmax(0)


def get_model():
    model = deeplabv3_resnet50(weights=DeepLabV3_ResNet50_Weights.DEFAULT)
    model.classifier = DeepLabHead(2048, N_CLASSES)
    model.to(DEVICE)
    return model


def train():
    trainloader, validloader, weight = get_dataset()
    print(len(trainloader), len(validloader))
    model = get_model()

    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = optim.AdamW(params=parameters, lr=LR, weight_decay=1e-1)
    criterion = nn.CrossEntropyLoss(weight=torch.tensor(weight).float()).to(DEVICE)
    lr_scheduler = optim.lr_scheduler.StepLR(optimizer=optimizer, step_size=10, gamma=0.9)

    trial_name = f"{NAME}_{datetime.now().strftime('%Y%m%d-%H%M%S')}"
    out_dir = f"out/{trial_name}/"
    os.makedirs(out_dir)
    copy2(os.path.realpath(__file__), out_dir)

    log = SummaryWriter(f"tb/{trial_name}/")
    train_csv = open(os.path.join(out_dir, "train.csv"), "a")
    print("Epoch,FrameNr,Video,dice", file=train_csv)
    valid_csv = open(os.path.join(out_dir, "valid.csv"), "a")
    print("Epoch,FrameNr,Video,dice", file=valid_csv)

    best_valid = -1
    try:
        for epoch in tqdm(range(N_EPOCHS), desc="Epoch", leave=False):
            model.train()
            losses = []
            dices = [[], [], [], [], [], [], []]
            for (frame_idx, video_idx), frames, masks in tqdm(trainloader, desc="Train", leave=False):
                scores = model(frames.to(DEVICE))['out']
                loss = criterion(scores, masks.long().to(DEVICE))
                losses.append(loss.item())

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                if epoch % 10 == 0 or epoch == N_EPOCHS - 1:
                    for i, video, score, mask in zip(
                            frame_idx, video_idx,
                            torch.argmax(scores, dim=1).cpu().numpy(),
                            masks.numpy()
                    ):
                        dice = f1_score(score.flatten(), mask.flatten(), labels=range(7), average=None, zero_division=0)
                        for i in range(7):
                            dices[i].append(dice[i])
                        print(f"{epoch},{i},{video},{dice}", file=train_csv)
            if epoch % 10 == 0 or epoch == N_EPOCHS - 1:
                log.add_scalars("Dice/Train",
                                {l: np.nanmean(dice) for l, dice in
                                 zip(["Background"] + [l.name for l in Label], dices)},
                                global_step=epoch)
            log.add_scalar("Loss/Train", np.nanmean(losses), global_step=epoch)

            model.eval()
            with torch.inference_mode():
                losses = []
                dices = [[], [], [], [], [], [], []]
                for (frame_idx, video_idx), frames, masks in tqdm(validloader, desc="Valid", leave=False):
                    scores = model(frames.to(DEVICE))['out']
                    loss = criterion(scores, masks.long().to(DEVICE))
                    losses.append(loss.item())

                    for i, video, score, mask in zip(
                            frame_idx, video_idx,
                            torch.argmax(scores, dim=1).cpu().numpy(),
                            masks.numpy()
                    ):
                        dice = f1_score(score.flatten(), mask.flatten(), labels=range(7), average=None, zero_division=0)
                        for i in range(7):
                            dices[i].append(dice[i])
                        print(f"{epoch},{i},{video},{dice}", file=valid_csv)

                dice = np.nanmean([np.nanmean(d) for d in dices])
                log.add_scalars("Dice/Valid",
                                {l: np.nanmean(dice) for l, dice in
                                 zip(["Background"] + [l.name for l in Label], dices)},
                                global_step=epoch)
                log.add_scalar("Loss/Valid", np.nanmean(losses), global_step=epoch)

                if dice > best_valid:
                    best_valid = dice
                    torch.save(model.state_dict(), os.path.join(out_dir, "best.pt"))

            lr_scheduler.step()

        torch.save(model.state_dict(), os.path.join(out_dir, "final.pt"))

    except Exception as e:
        print(e)

    finally:
        train_csv.close()
        valid_csv.close()
        log.close()


if __name__ == '__main__':
    train()
