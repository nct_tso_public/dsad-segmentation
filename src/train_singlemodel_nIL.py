import os
from datetime import datetime
from enum import IntEnum
from shutil import copy2

import numpy as np
import torch
from sklearn.metrics import f1_score, jaccard_score
from torch import optim, nn
from torch.utils.data import ConcatDataset, DataLoader
from torch.utils.tensorboard import SummaryWriter
from torchvision.models.segmentation import deeplabv3_resnet50, DeepLabV3_ResNet50_Weights
from torchvision.models.segmentation.deeplabv3 import DeepLabHead
from tqdm import tqdm

from dsad import DSAD_Video, train_transform
from split import split

ROOT = "/mnt/ceph/tco/TCO-Staff/Homes/jenkealex/DSAD_small/"
N_CLASSES = 7  # 6+1 Labels + Background
N_EPOCHS = 100
BATCH_SIZE = 8
LR = 1e-4

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class Label(IntEnum):
    AbdominalWall = 0
    Colon = 1
    Liver = 2
    Pancreas = 3
    SmallIntestine = 4
    Stomach = 5

    def dir(self):
        return [
            'abdominal_wall',
            'colon',
            'liver',
            'pancreas',
            'small_intestine',
            'stomach',
        ][self.value]


def get_dataset():
    trainsets = []
    validsets = []
    poss = []
    negs = []
    for label in Label:
        trainsets.append([DSAD_Video(ROOT, label, video, transform=train_transform) for video in split['train']])
        validsets.append([DSAD_Video(ROOT, label, video) for video in split['valid']])
        pos = 0
        l = 0
        for ds in trainsets[-1]:
            pos += ds.get_npos()
            l += len(ds)

        total = l * 640 * 512
        neg = total - pos
        poss.append(pos)
        negs.append(neg)
    trainset = ConcatDataset([ds for label_list in trainsets for ds in label_list])
    validset = ConcatDataset([ds for label_list in validsets for ds in label_list])

    trainloader = DataLoader(trainset, batch_size=BATCH_SIZE, num_workers=8, shuffle=True, pin_memory=True,
                             drop_last=True)
    validloader = DataLoader(validset, batch_size=BATCH_SIZE * 2, num_workers=8, shuffle=False, pin_memory=True)

    weights = [1]
    for pos, neg in zip(poss, negs):
        weights.append(neg / pos)

    # weights = [1, 5.03978634213041, 14.080789838663785, 64.31098395589399, 169.1001434450473, 6.7757717964996536,
    #            97.75953985186291, 10.786759006974751, 56.98134568533131, 36.28332962934047, 140.001397226702,
    #            82.03075602359812]
    print("Weight", weights)

    return trainloader, validloader, weights


def get_model():
    model = deeplabv3_resnet50(weights=DeepLabV3_ResNet50_Weights.DEFAULT)
    model.classifier = DeepLabHead(2048, N_CLASSES)
    model.to(DEVICE)
    return model


def train():
    trainloader, validloader, pos_weights = get_dataset()
    model = get_model()

    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = optim.AdamW(params=parameters, lr=LR, weight_decay=1e-1)
    criterion = nn.BCEWithLogitsLoss(pos_weight=torch.tensor(pos_weights).float(), reduction='none').to(DEVICE)
    lr_scheduler = optim.lr_scheduler.StepLR(optimizer=optimizer, step_size=10, gamma=0.9)

    trial_name = f"SingleHead_NoImplicitLabeling_6Classes_{datetime.now().strftime('%Y%m%d-%H%M%S')}"
    out_dir = f"out/{trial_name}/"
    os.makedirs(out_dir)
    copy2(os.path.realpath(__file__), out_dir)

    log = SummaryWriter(f"tb/{trial_name}/")
    train_csv = open(os.path.join(out_dir, "train.csv"), "a")
    print("Epoch,FrameNr,Video,Label,dice,iou", file=train_csv)
    valid_csv = open(os.path.join(out_dir, "valid.csv"), "a")
    print("Epoch,FrameNr,Video,Label,dice,iou", file=valid_csv)

    best_valid = -1
    try:
        for epoch in tqdm(range(N_EPOCHS), desc="Epoch", leave=False):
            model.train()
            losses = []
            dices = [[], [], [], [], [], [], []]
            ious = [[], [], [], [], [], [], []]
            for (frame_idx, video_idx, classes), frames, masks in tqdm(trainloader, desc="Train", leave=False):
                scores = model(frames.to(DEVICE))['out']
                loss_mask = torch.zeros_like(scores).cpu()
                loss_target = torch.zeros_like(scores).cpu().float()
                for i in range(masks.shape[0]):
                    lbl = classes[i].item() + 1
                    mask = masks[i].long()

                    l_msk = torch.zeros((N_CLASSES, mask.shape[0], mask.shape[1]))  # other classes ignored
                    l_msk[lbl] = torch.ones_like(mask)  # annotation only where annotated
                    loss_mask[i] = l_msk
                    loss_target[i][lbl] = mask

                    if epoch % 10 == 0 or epoch == N_EPOCHS - 1:
                        pred = (torch.argmax(scores[i], dim=0) == lbl).cpu().numpy()
                        dice = f1_score(pred.reshape(-1), mask.numpy().reshape(-1))
                        iou = jaccard_score(pred.reshape(-1), mask.numpy().reshape(-1))
                        dices[lbl].append(dice)
                        ious[lbl].append(iou)
                        print(f"{epoch},{frame_idx[i]},{video_idx[i]},{lbl},{dice},{iou}", file=train_csv)

                loss = (
                        criterion(
                            scores.permute(0, 2, 3, 1), loss_target.permute(0, 2, 3, 1).to(DEVICE)
                        ) * loss_mask.permute(0, 2, 3, 1).to(DEVICE)
                ).mean()
                losses.append(loss.item())

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            if epoch % 10 == 0 or epoch == N_EPOCHS - 1:
                log.add_scalars("Dice/Train",
                                {l: np.nanmean(dice) for l, dice in
                                 zip(["Background"] + [l.name for l in Label], dices)},
                                global_step=epoch)
                log.add_scalars("IoU/Train",
                                {l: np.nanmean(iou) for l, iou in zip(["Background"] + [l.name for l in Label], ious)},
                                global_step=epoch)
            log.add_scalar("Loss/Train", np.nanmean(losses), global_step=epoch)

            model.eval()
            with torch.inference_mode():
                losses = []
                dices = [[], [], [], [], [], [], []]
                ious = [[], [], [], [], [], [], []]
                for (frame_idx, video_idx, classes), frames, masks in tqdm(validloader, desc="Valid", leave=False):
                    scores = model(frames.to(DEVICE))['out']
                    loss_mask = torch.zeros_like(scores).cpu()
                    loss_target = torch.zeros_like(scores).cpu().float()
                    for i in range(masks.shape[0]):
                        lbl = classes[i].item() + 1
                        mask = masks[i].long()

                        l_msk = torch.zeros((N_CLASSES, mask.shape[0], mask.shape[1]))  # other classes ignored
                        l_msk[lbl] = torch.ones_like(mask)  # annotation only where annotated
                        loss_mask[i] = l_msk
                        loss_target[i][lbl] = mask

                        pred = (torch.argmax(scores[i], dim=0) == lbl).cpu().numpy()
                        dice = f1_score(pred.reshape(-1), mask.numpy().reshape(-1))
                        iou = jaccard_score(pred.reshape(-1), mask.numpy().reshape(-1))
                        dices[lbl].append(dice)
                        ious[lbl].append(iou)
                        print(f"{epoch},{frame_idx[i]},{video_idx[i]},{lbl},{dice},{iou}", file=train_csv)

                    loss = (
                            criterion(
                                scores.permute(0, 2, 3, 1), loss_target.permute(0, 2, 3, 1).to(DEVICE)
                            ) * loss_mask.permute(0, 2, 3, 1).to(DEVICE)
                    ).mean()
                    losses.append(loss.item())

                dice = np.nanmean([np.nanmean(d) for d in dices])
                log.add_scalars("Dice/Valid",
                                {l: np.nanmean(dice) for l, dice in
                                 zip(["Background"] + [l.name for l in Label], dices)},
                                global_step=epoch)
                log.add_scalars("IoU/Valid",
                                {l: np.nanmean(iou) for l, iou in zip(["Background"] + [l.name for l in Label], ious)},
                                global_step=epoch)
                log.add_scalar("Loss/Valid", np.nanmean(losses), global_step=epoch)

                if dice > best_valid:
                    best_valid = dice
                    torch.save(model.state_dict(), os.path.join(out_dir, "best.pt"))

            lr_scheduler.step()

        torch.save(model.state_dict(), os.path.join(out_dir, "final.pt"))

    except Exception as e:
        print(e)

    finally:
        train_csv.close()
        valid_csv.close()
        log.close()


if __name__ == '__main__':
    train()
