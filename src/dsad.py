import os.path
import pickle
from enum import IntEnum

import albumentations as A
import cv2
import matplotlib.pyplot as plt
import numpy as np
import torchvision.transforms as transforms
from albumentations.pytorch import ToTensorV2
from torch.utils.data import Dataset
from torch.utils.data.dataset import T_co
from tqdm import tqdm

from split import split


class FSS_Label(IntEnum):
    AbdominalWall = 0
    Colon = 1
    Liver = 2
    Pancreas = 3
    SmallIntestine = 4
    Stomach = 5

    def dir(self):
        return [
            'abdominal_wall',
            'colon',
            'liver',
            'pancreas',
            'small_intestine',
            'stomach',
        ][self.value]


class Label(IntEnum):
    AbdominalWall = 0
    Colon = 1
    InferiorMesentericArtery = 2
    IntestinalVeins = 3
    Liver = 4
    Pancreas = 5
    SmallIntestine = 6
    Spleen = 7
    Stomach = 8
    Ureter = 9
    VesicularGlands = 10

    def dir(self):
        return [
            'abdominal_wall',
            'colon',
            'inferior_mesenteric_artery',
            'intestinal_veins',
            'liver',
            'pancreas',
            'small_intestine',
            'spleen',
            'stomach',
            'ureter',
            'vesicular_glands',
        ][self.value]


denormalize = transforms.Normalize(mean=[-0.485 / 0.229, -0.456 / 0.224, -0.406 / 0.255],
                                   std=[1 / 0.229, 1 / 0.224, 1 / 0.255])

base_transform = A.Compose([
    A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    ToTensorV2(),
])

train_transform = A.Compose([
    A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=5, p=0.5),
    A.RGBShift(r_shift_limit=20, g_shift_limit=20, b_shift_limit=20, p=0.5),
    A.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=0.5),
    base_transform,
])


class DSAD_Video(Dataset):
    def __init__(self, root: str, label: Label, video: str,
                 mask_suffix: str = "",
                 transform=base_transform):
        self.transform = transform
        self.mask_suffix = mask_suffix
        self.label = label
        self.video = video

        self.dir = os.path.join(root, label.dir(), video)
        self.frames = []
        if os.path.isdir(self.dir):
            self.frames.extend([f[5:-4] for f in os.listdir(self.dir) if f.startswith("image") and f.endswith(".png")])

    def get_npos(self) -> float:
        n_pos = 0
        for i in self.frames:
            n_pos += (cv2.imread(
                os.path.join(self.dir, f"mask{i}{self.mask_suffix}.png"),
                cv2.IMREAD_GRAYSCALE
            ) > 0).astype(np.uint8).sum()

        return n_pos

    def __len__(self) -> int:
        return len(self.frames)

    def __getitem__(self, index) -> T_co:
        i = self.frames[index]
        frame = cv2.resize(
            cv2.cvtColor(cv2.imread(os.path.join(self.dir, f"image{i}.png")), cv2.COLOR_BGR2RGB),
            (640, 512)
        )
        mask = cv2.resize((cv2.imread(
            os.path.join(self.dir, f"mask{i}{self.mask_suffix}.png"), cv2.IMREAD_GRAYSCALE
        ) > 0).astype(np.uint8), (640, 512), interpolation=cv2.INTER_NEAREST)

        if self.transform is not None:
            transformed = self.transform(image=frame, mask=mask)
            frame = transformed['image']
            mask = transformed['mask']

        return (i, self.video, self.label), frame, mask


class DSAD_PseudoLabels(DSAD_Video):
    def __init__(self, img_root: str, mask_root: str,
                 label: Label, video: str,
                 mask_suffix: str = "",
                 transform=base_transform):
        super(DSAD_PseudoLabels, self).__init__(img_root, label, video, mask_suffix, transform)
        self.mask_dir = os.path.join(mask_root, label.dir(), video)

    def load_mask(self, path):
        with open(path, "rb") as f:
            data = pickle.load(f)
        return data

    def get_npos(self) -> float:
        n_pos = np.zeros(12)
        for i in self.frames:
            for j in range(12):
                m = self.load_mask(os.path.join(self.mask_dir, f"image{i}.pkl"))[0] == j
                n_pos[j] += m.astype(np.uint8).sum()
        return n_pos

    def __getitem__(self, index) -> T_co:
        i = self.frames[index]
        frame = cv2.cvtColor(cv2.imread(os.path.join(self.dir, f"image{i}.png")), cv2.COLOR_BGR2RGB)
        mask = (self.load_mask(os.path.join(self.mask_dir, f"image{i}.pkl"))[0]).astype(np.uint8)

        if self.transform is not None:
            transformed = self.transform(image=frame, mask=mask)
            frame = transformed['image']
            mask = transformed['mask']

        return (i, self.video, self.label), frame, mask


class DSAD_Multilabel(Dataset):
    def __init__(self, root: str, video: str,
                 transform=base_transform):
        self.transform = transform
        self.video = video
        self.dir = os.path.join(root, "multilabel", video)

        self.mask_cache = {}

        self.frames = []
        if os.path.isdir(self.dir):
            self.frames.extend([f[5:-4] for f in os.listdir(self.dir) if f.startswith("image") and f.endswith(".png")])

    def __len__(self) -> int:
        return len(self.frames)

    def get_mask(self, i):
        mask = np.zeros((12, 512, 640))

        for label in Label:
            file = os.path.join(self.dir, f"mask{i}_{label.dir()}.png")
            if not os.path.isfile(file): continue
            slice = cv2.resize(
                (cv2.imread(
                    file,
                    cv2.IMREAD_GRAYSCALE
                ) > 0).astype(np.uint8),
                (640, 512),
                interpolation=cv2.INTER_NEAREST
            )
            mask[label.value + 1] = slice

        return mask.argmax(axis=0)

    def __getitem__(self, index) -> T_co:
        i = self.frames[index]

        frame = cv2.resize(
            cv2.cvtColor(cv2.imread(os.path.join(self.dir, f"image{i}.png")), cv2.COLOR_BGR2RGB),
            (640, 512)
        )

        if i not in self.mask_cache:
            self.mask_cache[i] = self.get_mask(i)
        mask = self.mask_cache[i]

        if self.transform is not None:
            transformed = self.transform(image=frame, mask=mask)
            frame = transformed['image']
            mask = transformed['mask']

        return (i, self.video), frame, mask


class DSAD_FSS(Dataset):
    def __init__(self, root: str, video: str,
                 transform=base_transform,
                 only_label: Label = None,
                 ):
        self.transform = transform
        self.video = video
        self.dir = os.path.join(root, "multilabel", video)

        self.mask_cache = {}
        self.only_label = only_label

        self.frames = []
        if os.path.isdir(self.dir):
            self.frames.extend([f[5:-4] for f in os.listdir(self.dir) if f.startswith("image") and f.endswith(".png")])

    def __len__(self) -> int:
        return len(self.frames)

    def get_npos(self):
        if self.only_label is not None:
            n_pos = 0
            for i in self.frames:
                n_pos += self.get_mask(i).sum()
        else:
            n_pos = np.zeros(7)
            for i in self.frames:
                mask = self.get_mask(i)
                for j in range(7):
                    n_pos[j] += (mask == j).sum()

        return n_pos

    def get_spleen(self):
        n_pos = 0
        for i in self.frames:
            mask = cv2.imread(
                os.path.join(self.dir, f"mask{i}_spleen.png"),
                cv2.IMREAD_GRAYSCALE
            ).astype(np.uint8)
            n_pos += (mask > 0).sum()
        return n_pos

    """
4 17680
12 4269697
22 50998
23 27880
24 1292907
"""

    def get_mask(self, i):
        if i not in self.mask_cache:
            if self.only_label is not None:  # return only one label if specified
                label = self.only_label
                file = os.path.join(self.dir, f"mask{i}_{label.dir()}.png")
                mask = np.zeros((1, 512, 640))
                if os.path.isfile(file):
                    slice = cv2.resize(
                        (cv2.imread(
                            file,
                            cv2.IMREAD_GRAYSCALE
                        ) > 0).astype(np.uint8),
                        (640, 512),
                        interpolation=cv2.INTER_NEAREST
                    )
                    mask[0] = slice

                self.mask_cache[i] = mask[0]
            else:
                mask = np.zeros((7, 512, 640))

                for label in FSS_Label:
                    file = os.path.join(self.dir, f"mask{i}_{label.dir()}.png")
                    if not os.path.isfile(file): continue
                    slice = cv2.resize(
                        (cv2.imread(
                            file,
                            cv2.IMREAD_GRAYSCALE
                        ) > 0).astype(np.uint8),
                        (640, 512),
                        interpolation=cv2.INTER_NEAREST
                    )
                    mask[label.value + 1] = slice

                self.mask_cache[i] = mask.argmax(axis=0)

        return self.mask_cache[i]

    def __getitem__(self, index) -> T_co:
        i = self.frames[index]

        frame = cv2.resize(
            cv2.cvtColor(cv2.imread(os.path.join(self.dir, f"image{i}.png")), cv2.COLOR_BGR2RGB),
            (640, 512)
        )

        mask = self.get_mask(i)

        if self.transform is not None:
            transformed = self.transform(image=frame, mask=mask)
            frame = transformed['image']
            mask = transformed['mask']

        if self.only_label is not None:
            return (i, self.video, self.only_label), frame, mask
        return (i, self.video), frame, mask


if __name__ == '__main__':
    from torch.utils.data import ConcatDataset, DataLoader

    root = "/mnt/ceph/tco/TCO-All/SharedDatasets/DSAD/"  # _small/"
    mask_root = "/mnt/ceph/tco/TCO-Staff/Homes/jenkealex/DSAD/multimodel/"
    for s in ['train', 'valid', 'test']:
        n_pos = np.zeros(7)
        for i in split[s]:
            print(i)
            n_pos += DSAD_FSS(root, str(i).zfill(2)).get_npos()
        print(s, n_pos)

    trainsets = []
    for label in [Label.VesicularGlands]:
        trainsets.append(
            [DSAD_Multilabel(root, video) for video in split['train']])

    trainset = ConcatDataset([ds for label_list in trainsets for ds in label_list])

    trainloader = DataLoader(trainset, batch_size=4, num_workers=8, shuffle=False, pin_memory=True,
                             drop_last=True)

    for details, frames, masks in tqdm(trainloader, desc="Train", leave=False):
        for frame, mask in zip(frames, masks):
            fig, axs = plt.subplots(1, 2)
            axs[0].imshow(frame.permute(1, 2, 0))
            axs[1].imshow(mask)
            plt.show()
