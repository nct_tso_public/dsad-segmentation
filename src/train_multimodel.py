import os
from datetime import datetime
from shutil import copy2

import numpy as np
import torch
from sklearn.metrics import f1_score, jaccard_score
from torch import optim, nn
from torch.utils.data import ConcatDataset, DataLoader
from torch.utils.tensorboard import SummaryWriter
from torchvision.models.segmentation import deeplabv3_resnet50, DeepLabV3_ResNet50_Weights
from torchvision.models.segmentation.deeplabv3 import DeepLabHead
from tqdm import tqdm

from dsad import DSAD_Video, train_transform, Label
from split import split

ROOT = "/path/to/dataset/"
N_CLASSES = 2
N_EPOCHS = 100
BATCH_SIZE = 8
LR = 3e-4

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def get_dataset(label: Label):
    trainset = ConcatDataset([DSAD_Video(ROOT, label, video, transform=train_transform) for video in split['train']])
    validset = ConcatDataset([DSAD_Video(ROOT, label, video) for video in split['valid']])
    testset = ConcatDataset([DSAD_Video(ROOT, label, video) for video in split['test']])

    trainloader = DataLoader(trainset, batch_size=BATCH_SIZE, num_workers=8, shuffle=True, pin_memory=True,
                             drop_last=True)
    validloader = DataLoader(validset, batch_size=32, num_workers=8, shuffle=False, pin_memory=True)
    testloader = DataLoader(testset, batch_size=32, num_workers=8, shuffle=False, pin_memory=True)

    pos = 0
    for ds in trainset.datasets:
        pos += ds.get_npos()

    total = len(trainset) * 640 * 512
    neg = total - pos

    return trainloader, validloader, testloader, neg / pos


def get_model():
    model = deeplabv3_resnet50(weights=DeepLabV3_ResNet50_Weights.DEFAULT)
    model.classifier = DeepLabHead(2048, N_CLASSES)
    model.to(DEVICE)
    return model


def train_label(label: Label):
    trainloader, validloader, testloader, pos_weight = get_dataset(label)
    model = get_model()

    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = optim.AdamW(params=parameters, lr=LR, weight_decay=1e-1)
    criterion = nn.CrossEntropyLoss(weight=torch.tensor([1, pos_weight]).float()).to(DEVICE)
    lr_scheduler = optim.lr_scheduler.StepLR(optimizer=optimizer, step_size=10, gamma=0.9)

    trial_name = f"MultiModel_{label.name}_{datetime.now().strftime('%Y%m%d-%H%M%S')}"
    out_dir = f"out/{trial_name}/"
    os.makedirs(out_dir)
    copy2(os.path.realpath(__file__), out_dir)

    log = SummaryWriter(f"tb/{trial_name}/")
    train_csv = open(os.path.join(out_dir, "train.csv"), "a")
    print("Epoch,FrameNr,Video,dice,iou", file=train_csv)
    valid_csv = open(os.path.join(out_dir, "valid.csv"), "a")
    print("Epoch,FrameNr,Video,dice,iou", file=valid_csv)
    test_csv = open(os.path.join(out_dir, "test.csv"), "a")
    print("Epoch,FrameNr,Video,dice,iou", file=test_csv)

    best_valid = -1
    epoch = 0
    try:
        for epoch in tqdm(range(N_EPOCHS), desc="Epoch", leave=False):
            model.train()
            losses = []
            dices = []
            ious = []
            for details, frames, masks in tqdm(trainloader, desc="Train", leave=False):
                scores = model(frames.to(DEVICE))['out']
                loss = criterion(scores, masks.long().to(DEVICE))
                losses.append(loss.item())

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                for i, video, score, mask in zip(
                        details[0], details[1],
                        (torch.argmax(scores, dim=1) > 0).cpu().numpy(),
                        masks.numpy()
                ):
                    dice = f1_score(score.reshape(-1), mask.reshape(-1))
                    iou = jaccard_score(score.reshape(-1), mask.reshape(-1))
                    dices.append(dice)
                    ious.append(iou)
                    print(f"{epoch},{i},{video},{dice},{iou}", file=train_csv)

            log.add_scalar("Dice/Train", np.nanmean(dices), global_step=epoch)
            log.add_scalar("IoU/Train", np.nanmean(ious), global_step=epoch)
            log.add_scalar("Loss/Train", np.nanmean(losses), global_step=epoch)

            model.eval()
            with torch.inference_mode():
                losses = []
                dices = []
                ious = []
                for details, frames, masks in tqdm(validloader, desc="Valid", leave=False):
                    scores = model(frames.to(DEVICE))['out']
                    loss = criterion(scores, masks.long().to(DEVICE))
                    losses.append(loss.item())

                    for i, video, score, mask in zip(
                            details[0], details[1],
                            (torch.argmax(scores, dim=1) > 0).cpu().numpy(),
                            masks.numpy()
                    ):
                        dice = f1_score(score.reshape(-1), mask.reshape(-1))
                        iou = jaccard_score(score.reshape(-1), mask.reshape(-1))
                        dices.append(dice)
                        ious.append(iou)
                        print(f"{epoch},{i},{video},{dice},{iou}", file=valid_csv)

                dice = np.nanmean(dices)
                log.add_scalar("Dice/Valid", dice, global_step=epoch)
                log.add_scalar("IoU/Valid", np.nanmean(ious), global_step=epoch)
                log.add_scalar("Loss/Valid", np.nanmean(losses), global_step=epoch)

                if dice > best_valid:
                    best_valid = dice
                    torch.save(model.state_dict(), os.path.join(out_dir, "best.pt"))

            lr_scheduler.step()

        torch.save(model.state_dict(), os.path.join(out_dir, "final.pt"))

        model.load_state_dict(torch.load(os.path.join(out_dir, "best.pt")))
        model.to(DEVICE)
        model.eval()
        with torch.inference_mode():
            losses = []
            dices = []
            ious = []
            for details, frames, masks in tqdm(testloader, desc="Test", leave=False):
                scores = model(frames.to(DEVICE))['out']
                loss = criterion(scores, masks.long().to(DEVICE))
                losses.append(loss.item())

                for i, video, score, mask in zip(
                        details[0], details[1],
                        (torch.argmax(scores, dim=1) > 0).cpu().numpy(),
                        masks.numpy()
                ):
                    dice = f1_score(score.reshape(-1), mask.reshape(-1))
                    iou = jaccard_score(score.reshape(-1), mask.reshape(-1))
                    dices.append(dice)
                    ious.append(iou)
                    print(f"{epoch},{i},{video},{dice},{iou}", file=test_csv)

            dice = np.nanmean(dices)
            log.add_scalar("Dice/Test", dice, global_step=epoch)
            log.add_scalar("IoU/Test", np.nanmean(ious), global_step=epoch)
            log.add_scalar("Loss/Test", np.nanmean(losses), global_step=epoch)

    except Exception as e:
        print(e)

    finally:
        train_csv.close()
        valid_csv.close()
        test_csv.close()
        log.close()


def run():
    for label in Label:
        print(label.name)
        train_label(label=label)


if __name__ == '__main__':
    run()
