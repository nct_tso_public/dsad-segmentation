import time
from torch.nn.utils.stateless import functional_call
import numpy as np
import torch
from torchvision.models.segmentation import deeplabv3_resnet50
from torchvision.models.segmentation.deeplabv3 import DeepLabHead, DeepLabV3_ResNet50_Weights

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
i = torch.randn((1, 3, 512, 640))
REP = 1001

def time_mCls():
    mCls = deeplabv3_resnet50(weights=DeepLabV3_ResNet50_Weights.DEFAULT)
    mCls.classifier = DeepLabHead(2048, 7)
    mCls.to(DEVICE)
    mCls.load_state_dict(torch.load(
        "models/MultiClass.pt",
        map_location=DEVICE))
    mCls.eval()

    times = []
    with torch.inference_mode():
        for _ in range(REP):
            torch.cuda.synchronize()
            t1 = time.time_ns()
            mCls(i.to(DEVICE))['out'].argmax(dim=1).cpu()
            torch.cuda.synchronize()
            t2 = time.time_ns()
            times.append((t2-t1)/1000000)
    print("mCls", np.median(times[1:]), np.mean(times[1:]), np.std(times[1:]))

def time_mMdl():
    mMdl = deeplabv3_resnet50(weights=DeepLabV3_ResNet50_Weights.DEFAULT)
    mMdl.classifier = DeepLabHead(2048, 2)
    mMdl.to(DEVICE)
    mMdl.eval()
    sig = torch.nn.Sigmoid()

    mMdl_sd = [torch.load(p, map_location=DEVICE) for p in [
        "models/MultiModel_AbdominalWall.pt",
        "models/MultiModel_Colon.pt",
        "models/MultiModel_Liver.pt",
        "models/MultiModel_Pancreas.pt",
        "models/MultiModel_SmallIntestine.pt",
        "models/MultiModel_Stomach.pt",
    ]]

    times = []
    with torch.inference_mode():
        for _ in range(REP):
            torch.cuda.synchronize()
            t1 = time.time_ns()
            stack = torch.ones((1, 7, 512, 640), device=DEVICE) * .5
            ii = i.to(DEVICE)
            for j, sd in enumerate(mMdl_sd):
                stack[:, j + 1] = sig(
                    functional_call(
                        mMdl,
                        sd,
                        ii
                    )['out'][:, 1]
                )
            _ = stack.argmax(dim=1).cpu()
            torch.cuda.synchronize()
            t2 = time.time_ns()
            times.append((t2 - t1) / 1000000)
        print("mMdl", np.median(times[1:]), np.mean(times[1:]), np.std(times[1:]))


def time_sMdl():
    sMdl = deeplabv3_resnet50(weights=DeepLabV3_ResNet50_Weights.DEFAULT)
    sMdl.classifier = DeepLabHead(2048, 7)
    sMdl.to(DEVICE)
    sMdl.load_state_dict(torch.load(
        "models/SingleModel.pt",
        map_location=DEVICE))
    sMdl.eval()

    times = []
    with torch.inference_mode():
        for _ in range(REP):
            torch.cuda.synchronize()
            t1 = time.time_ns()
            sMdl(i.to(DEVICE))['out'].argmax(dim=1).cpu()
            torch.cuda.synchronize()
            t2 = time.time_ns()
            times.append((t2-t1)/1000000)
    print("sMdl", np.median(times[1:]), np.mean(times[1:]), np.std(times[1:]))


if __name__ == '__main__':
    print(DEVICE)
    time_mCls()
    time_mMdl()
    time_sMdl()
    time_mCls()
    time_mMdl()
    time_sMdl()
    time_mCls()
    time_mMdl()
    time_sMdl()
