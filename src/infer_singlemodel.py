import os

import cv2
import numpy as np
import torch
from torch.utils.data import ConcatDataset, DataLoader
from torchvision.models.segmentation import deeplabv3_resnet50
from torchvision.models.segmentation.deeplabv3 import DeepLabHead, DeepLabV3_ResNet50_Weights
from tqdm import tqdm

from dsad import DSAD_FSS, DSAD_Video, Label
from split import split

ROOT = "/path/to/dataset/"  # modify as needed
OUTPUT = "/path/to/output/"  # modify as needed
N_CLASSES = 7
BATCH_SIZE = 12

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def get_dataset(label=None):
    if label is None:
        ds = ConcatDataset([DSAD_FSS(ROOT, video) for video in split['train'] + split['valid'] + split['test']])
    else:
        ds = ConcatDataset([DSAD_Video(ROOT, label, video) for video in split['train'] + split['valid'] + split['test']])

    dl = DataLoader(ds, batch_size=BATCH_SIZE, num_workers=8, shuffle=False, pin_memory=True)
    return dl


def get_model(path: str):
    model = deeplabv3_resnet50(weights=DeepLabV3_ResNet50_Weights.DEFAULT)
    model.classifier = DeepLabHead(2048, N_CLASSES)
    model.to(DEVICE)
    model.load_state_dict(torch.load(path, map_location=DEVICE))
    model.eval()
    return model


def infer(label:Label=None):
    model = get_model(
        "models/SingleModel.pt")
    dl = get_dataset(label)

    if label is None:
        label = "multiclass"
    else:
        label = label.dir()

    with torch.inference_mode():
        for (idx, videos, *_), frames, masks in tqdm(dl, desc=f"Infer"):
            scores = model(frames.to(DEVICE))['out']
            scores[:, 0] = 0  # if not sure about oher classes choose BG
            preds = scores.argmax(dim=1).cpu().numpy()

            for id, v, pred in zip(idx, videos, preds):
                img = np.zeros((512, 640, 3))
                colors = [(0, 0, 0), (1, 255, 0), (2, 0, 255), (3, 255, 255), (4, 125, 0), (5, 0, 125), (6, 125, 125)]
                for i in range(1, 7):
                    img[pred == i] = colors[i]

                os.makedirs(
                    f"{OUTPUT}fssds_singlemodel/{label}/{v}",
                    exist_ok=True
                )
                cv2.imwrite(
                    f"{OUTPUT}fssds_singlemodel/{label}/{v}/mask{id}.png",
                    img
                )


if __name__ == '__main__':
    infer()  # multiclass
    for label in Label:
        infer(label)  # binary
