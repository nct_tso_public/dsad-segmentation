from collections import namedtuple

import numpy as np
import torch
from torch import Tensor

Metrics = namedtuple("Metrics", "precision recall f1 specificity npv jaccard accuracy")

def calculate_metrics(y_true: Tensor, y_pred: Tensor) -> Metrics:
    y_true = y_true.bool()
    y_pred = y_pred.bool()
    matches = y_pred == y_true
    unmatches = y_pred != y_true
    positives = y_true == True
    negatives = y_true == False

    dims = tuple(range(1, y_true.dim()))  # which dimensions to summarize (all except the first)

    tp = torch.sum(matches * positives, dim=dims)  # true positives
    tn = torch.sum(matches * negatives, dim=dims)  # true negatives
    fn = torch.sum(unmatches * positives, dim=dims)  # false negatives
    fp = torch.sum(unmatches * negatives, dim=dims)  # false negatives
    p = torch.sum(positives)

    ppv = tp / (fp + tp)  # positive predictive value / precision
    tpr = tp / (tp + fn)  # true positive rate / recall / senitivity
    f1 = tp / (tp + 0.5 * (fp + fn))  # f1 score
    tnr = tn / (tn + fp)  # true negative rate / specificity
    npv = tn / (tn + fn)  # negative predictive value
    ts = tp / (tp + fp + fn)  # threat score / jaccard index
    acc = (tp + tn) / (tp + fp + fn + tn)  # accuracy

    return Metrics(precision=ppv, recall=tpr, f1=f1, specificity=tnr, npv=npv, jaccard=ts, accuracy=acc)


def pred_by_class(y_score: Tensor, classes: Tensor, th=.5) -> Tensor:
    assert y_score.shape[0] == classes.shape[0]
    return torch.stack([img[cls] > th for img, cls in zip(y_score, classes)], dim=0)


def pred_by_ensemble(y_score: Tensor, classes: Tensor, th=0) -> Tensor:
    value, idx = y_score.max(dim=1)
    return torch.stack([(i == cls) * (v > th) for v, i, cls in zip(value, idx, classes)], dim=0)


def binarize_ensemble(ensemble: Tensor, classes: Tensor) -> Tensor:
    assert ensemble.shape[0] == classes.shape[0]
    return torch.stack([img == cls for img, cls in zip(ensemble, classes)], dim=0)
