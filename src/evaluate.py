import os

import cv2
import numpy as np
import torch

import metrics

ROOT = "/path/to/output/"


def init_csv():
    with open(f"{ROOT}/evaluate_models.csv", "w") as out:
        print("Dataset,Model,Label,Video,Frame,F1,Precision,Recall,IoU,Specificity,Accuracy", file=out)


def load_mask(path):
    return np.asarray(cv2.imread(path)[:, :, 0])


def eval_binary(model):
    prec = []
    rec = []
    f1 = []
    iou = []

    with open(f"{ROOT}/evaluate_models.csv", "a") as out:
        for i, label in enumerate(['abdominal_wall', 'colon', 'liver', 'pancreas', 'small_intestine', 'stomach']):
            p, r, f, j = [], [], [], []
            for video in ['02', '07', '11', '13', '14', '18', '20', '32']:
                if not os.path.isdir(os.path.join(ROOT, "gt", label, video)): continue
                for frame in [f for f in os.listdir(os.path.join(ROOT, "gt", label, video)) if
                              f.startswith("mask") and f.endswith(".png")]:
                    gt = load_mask(os.path.join(ROOT, "gt", label, video, frame)) > 0
                    pred = load_mask(os.path.join(ROOT, model, label, video, frame))

                    m = metrics.calculate_metrics(y_true=torch.tensor(gt.flatten()),
                                                  y_pred=torch.tensor((pred == i + 1).flatten()))
                    print(
                        f"Binary,{model},{label},{video},{frame},{m.f1},{m.precision},{m.recall},{m.jaccard},{m.specificity},{m.accuracy}",
                        file=out)

                    p.append(m.precision)
                    r.append(m.recall)
                    f.append(m.f1)
                    j.append(m.jaccard)
            prec.append(p)
            rec.append(r)
            f1.append(f)
            iou.append(j)

    print("F1:       ", ", ".join([f"{np.nanmean(l):.3f}" for l in f1]),
          f"({np.mean([np.nanmean(l) for l in f1]):.3f})", "\t",
          "Prec:       ", ", ".join([f"{np.nanmean(l):.3f}" for l in prec]),
          f"({np.mean([np.nanmean(l) for l in prec]):.3f})",
          "\t",
          "Rec:       ", ", ".join([f"{np.nanmean(l):.3f}" for l in rec]),
          f"({np.mean([np.nanmean(l) for l in rec]):.3f})",
          "\t",
          "IoU:       ", ", ".join([f"{np.nanmean(l):.3f}" for l in iou]),
          f"({np.mean([np.nanmean(l) for l in iou]):.3f})",
          "\t",
          model)


def eval_multiclass(model):
    prec, rec, f1, iou = [], [], [], []
    with open(f"{ROOT}/evaluate_models.csv", "a") as out:
        for video in ['02', '07', '11', '18', '20']:
            for frame in [f for f in os.listdir(os.path.join(ROOT, "gt", "multiclass", video)) if f.endswith(".png")]:
                gt = load_mask(os.path.join(ROOT, "gt", "multiclass", video, frame))
                pred = load_mask(os.path.join(ROOT, model, "multiclass", video, frame))

                ms = []
                for i in range(7):
                    m = metrics.calculate_metrics(y_true=torch.tensor((gt == i).flatten()),
                                                  y_pred=torch.tensor((pred == i).flatten()))
                    ms.append(m)
                    label = ['background', 'abdominal_wall', 'colon', 'liver',
                             'pancreas', 'small_intestine', 'stomach'][i]
                    print(
                        f"Multiclass,{model},{label},{video},{frame},{m.f1},{m.precision},{m.recall},{m.jaccard},{m.specificity},{m.accuracy}",
                        file=out)

                prec.append([m.precision for m in ms])
                rec.append([m.recall for m in ms])
                f1.append([m.f1 for m in ms])
                iou.append([m.jaccard for m in ms])

    prec = np.asarray(prec)
    rec = np.asarray(rec)
    f1 = np.asarray(f1)
    iou = np.asarray(iou)
    print("F1:", ", ".join([f"{v:.3f}" for v in np.nanmean(f1, axis=0)]), f"({np.nanmean(f1, axis=0).mean():.3f})",
          "\t",
          "Prec:", ", ".join([f"{v:.3f}" for v in np.nanmean(prec, axis=0)]),
          f"({np.nanmean(prec, axis=0).mean():.3f})", "\t",
          "Rec:", ", ".join([f"{v:.3f}" for v in np.nanmean(rec, axis=0)]), f"({np.nanmean(rec, axis=0).mean():.3f})",
          "\t",
          "IoU:", ", ".join([f"{v:.3f}" for v in np.nanmean(iou, axis=0)]), f"({np.nanmean(iou, axis=0).mean():.3f})",
          "\t",
          model)


if __name__ == '__main__':
    init_csv()
    print("\nBinary:")
    for model in ['multimodel', 'singlemodel', 'multiclass']:
        eval_binary(model)

    print("\nMulticlass:")
    for model in ['multimodel', 'singlemodel', 'multiclass']:
        eval_multiclass(model)
