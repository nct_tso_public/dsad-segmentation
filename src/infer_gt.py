import os

import cv2
import numpy as np
import torch
from torch.utils.data import ConcatDataset, DataLoader
from tqdm import tqdm

from dsad import DSADMultiClass, DSADSingleLabel, SingleClassLabel
from split import split

ROOT = "/path/to/dataset/"  # modify as needed
OUTPUT = "/path/to/output/"  # modify as needed
BATCH_SIZE = 24

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def get_dataset(label=None):
    if label is None:
        ds = ConcatDataset([DSADMultiClass(ROOT, video) for video in split['train'] + split['valid'] + split['test']])
    else:
        ds = ConcatDataset(
            [DSADSingleLabel(ROOT, label, video) for video in split['train'] + split['valid'] + split['test']])

    dl = DataLoader(ds, batch_size=BATCH_SIZE, num_workers=8, shuffle=False, pin_memory=True)
    return dl


def infer(label: SingleClassLabel = None):
    if label is None:
        label = "multiclass"
    else:
        label = label.dir()

    dl = get_dataset()
    for (idx, videos), frames, masks in tqdm(dl, desc=f"Infer"):
        for id, v, pred in zip(idx, videos, masks):
            img = np.zeros((512, 640, 3))
            colors = [(0, 0, 0), (1, 255, 0), (2, 0, 255), (3, 255, 255), (4, 125, 0), (5, 0, 125), (6, 125, 125)]
            if label == "multiclass":
                for i in range(1, 7):
                    img[pred == i] = colors[i]
            else:
                img[pred == 1] = colors[{
                    'abdominal_wall': 1,
                    'colon': 2,
                    'liver': 3,
                    'pancreas': 4,
                    'small_intestine': 5,
                    'stomach': 6,
                }[label]]

                os.makedirs(
                    f"{OUTPUT}gt/{label}/{v}",
                    exist_ok=True
                )
                cv2.imwrite(
                    f"{OUTPUT}gt/{label}/{v}/mask{id}.png",
                    img
                )


if __name__ == '__main__':
    infer()  # multiclass
    for label in SingleClassLabel:
        infer(label)  # binary
